module.exports = function debounce(func, wait, immediate){
  var context,
    args,
    result,
    timeout, 
    lastRun = 0;
  immediate = (immediate ? true : false);
	  
  if(null == wait) wait = 100;

  function later() {
    var last = (lastRun > 0 ? Date.now() - lastRun : 0);

    if (last < wait && last > 0) {
      timeout = setTimeout(later, wait - last);
    } else {
      timeout = null;
      lastRun = Date.now();
      result = func.apply(context, args);
      context = args = null;
    }
  }

  return function debounced() {
    var last = (lastRun > 0 ? Date.now() - lastRun : 0);
	
    context = this;
    args = arguments;

    if(!timeout) {
      if(immediate) {
        if(last === 0 || last >= wait) {
          lastRun = Date.now();
          result = func.apply(context, args);
          context = args = null;
        } else {
          timeout = setTimeout(later, wait - last);
        }
      } else {
        timeout = setTimeout(later, wait);
      }
    }

    return result;
  };
};